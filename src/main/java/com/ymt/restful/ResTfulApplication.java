package com.ymt.restful;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.stereotype.Controller;

@Configuration
@EnableAutoConfiguration
@ComponentScan
@Controller
public class ResTfulApplication {

	public static void main(String[] args) {
		SpringApplication.run(ResTfulApplication.class, args);
	}
}
