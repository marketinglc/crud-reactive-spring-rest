package com.ymt.restful.dao;

import com.ymt.restful.entity.User;
import com.ymt.restful.repository.UserRepository;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

import java.util.HashMap;
import java.util.Map;

public class UserRepositorySample implements UserRepository {

    // Iniciar usuarios
    private final Map<Integer, User> users = new HashMap<>();

    // llenar valores ficticios para prueba
    public UserRepositorySample() {
        //Maps inmutables usando en Java 9
        /*users = Map.of(
                1, (new User(1, "Mich")),
                2, (new User(2, "Bruno")),
                3, (new User(3, "Jesus"))
        );*/
        this.users.put(100, new User(100, "Mich"));
        this.users.put(101, new User(101, "Bruno"));
        this.users.put(102, new User(102, "Jesus"));
    }

    //este método será para retornar todos los ususarios
    @Override
    public Flux<User> getAllUsers() {
        return Flux.fromIterable(this.users.values());
    }

    @Override
    public Mono<User> getUser(Integer id) {
        return Mono.justOrEmpty(this.users.get(id));
    }

    @Override
    public Mono<Void> saveUser(Mono<User> userMono) {
        return userMono.doOnNext(user -> {
            users.put(user.getUserid(), user);
            System.out.format("Saved %s with id %d%n", user, user.getUserid());
        }).thenEmpty(Mono.empty());
    }

    @Override
    public Mono<Void> updateUser(Mono<User> userMono) {
        return userMono.doOnNext(user -> {
            users.put(user.getUserid(), user);
            System.out.format("Saved %s with id %d%n", user, user.getUserid());
        }).thenEmpty(Mono.empty());
    }

    @Override
    public Mono<Void> deleteUser(Integer id) {

        users.remove(id);
        System.out.println("user : "+users);

        return Mono.empty();
    }
}
